package ro.softvision.avioane.player;

import ro.softvision.avioane.board.Board;
import ro.softvision.avioane.board.Coordinate;
import ro.softvision.avioane.item.Plane;
import ro.softvision.avioane.item.Ship;

import java.io.IOException;

/**
 * Created by mihaly.nagy on 10/03/17.
 */
public abstract class Player {

    private static final int BOARD_SIZE = 10;
    private String name;
    protected Board playerBoard;

    protected Player(String name) {
        this.name = name;
        playerBoard = new Board(BOARD_SIZE);
    }

    public Board getPlayerBoard() {
        return playerBoard;
    }

    public String getName() {
        return name;
    }

    public void configureBoard(int numOfObjects) throws IOException {
        while (playerBoard.getPlaneCount() < numOfObjects) {
            Coordinate coords = inputCoordinates();
            Ship.Orientation orientation = inputOrientation();

            Plane plane = Plane.createPlane(coords, orientation);

            if (!playerBoard.isPlaneOnBoard(plane)) {
                System.out.println("Plane is not on board! " + plane);
                continue;
            }

            if (playerBoard.isOverlappingOtherPlane(plane)) {
                System.out.println("Plane is overlapping existing plane on board! " + plane);
                continue;
            }

            playerBoard.addPlane(plane);
            displayBoard();
        }
    }

    public abstract Coordinate inputCoordinates() throws IOException;

    protected abstract Ship.Orientation inputOrientation() throws IOException;

    public abstract void displayBoard();
}
