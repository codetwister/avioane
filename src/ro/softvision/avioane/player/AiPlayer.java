package ro.softvision.avioane.player;

import ro.softvision.avioane.board.Coordinate;
import ro.softvision.avioane.item.Ship;

/**
 * Created by mihaly.nagy on 10/03/17.
 */
public class AiPlayer extends Player {
    public AiPlayer(String name) {
        super(name);
    }

    @Override
    public Coordinate inputCoordinates() {
        int x = (int) (Math.random() * playerBoard.getBoardSize() - 1) + 1;
        int y = (int) (Math.random() * playerBoard.getBoardSize() - 1) + 1;
        return new Coordinate(x, y);
    }

    @Override
    protected Ship.Orientation inputOrientation() {
        int i = (int) (Math.random() * Ship.Orientation.values().length);
        return Ship.Orientation.values()[i];
    }

    @Override
    public void displayBoard() {
        // nothing to do here
    }
}
