package ro.softvision.avioane.player;

import ro.softvision.avioane.board.Coordinate;
import ro.softvision.avioane.item.Ship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by mihaly.nagy on 10/03/17.
 */
public class HumanPlayer extends Player {

    public HumanPlayer(String name) {
        super(name);
    }

    @Override
    public Coordinate inputCoordinates() throws IOException {
        int x = inputOneCoordinate("X");
        int y = inputOneCoordinate("Y");
        return new Coordinate(x, y);
    }

    private int inputOneCoordinate(String name) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int coordinate = -1;
        while (coordinate < 0) {
            System.out.println("Enter " + name + " coordinate: ");
            String coordinateString = br.readLine().trim();
            try {
                coordinate = Integer.parseInt(coordinateString);
            } catch (NumberFormatException e) {
                System.out.println("Invalid coordinate!");
            }
        }
        return coordinate;
    }

    @Override
    protected Ship.Orientation inputOrientation() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.print("Enter plane's orientation(N, S, W, E): ");
            String orientation = br.readLine().trim();
            try {
                return Ship.Orientation.valueOf(orientation.toUpperCase());
            } catch (IllegalArgumentException e) {
                System.out.println("Invalid orientation");
            }
        } while (true);
    }

    @Override
    public void displayBoard() {
        getPlayerBoard().printSelf();
    }
}
