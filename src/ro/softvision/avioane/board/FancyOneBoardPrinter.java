package ro.softvision.avioane.board;

import ro.softvision.avioane.board.boxchar.BoxCharBuffer;
import ro.softvision.avioane.item.Plane;
import ro.softvision.avioane.item.Ship;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by codetwister on 3/29/17.
 */
public class FancyOneBoardPrinter {

    private static final int PADDING_FOR_TEXT = 3;

    private int boardBufferSizeWidth;
    private int boardBufferSizeHeight;

    private int boardSize;
    private int gridSize;

    private BoxCharBuffer boxCharBuffer;

    private FancyPlanePrinter fancyPlanePrinter;

    public FancyOneBoardPrinter(int boardSize, int gridSize) {
        this.boardSize = boardSize;
        this.gridSize = gridSize;

        boardBufferSizeHeight = PADDING_FOR_TEXT + (boardSize - 1) * gridSize;
        boardBufferSizeWidth = boardBufferSizeHeight;

        boxCharBuffer = new BoxCharBuffer(boardBufferSizeWidth, boardBufferSizeHeight);

        fancyPlanePrinter = new FancyPlanePrinter(boardSize, gridSize, boxCharBuffer);

        addGrid();

        addGridNumbers();
    }

    private void addGridNumbers() {
        int gridPaddingLeft = PADDING_FOR_TEXT;
        int gridPaddingTop = PADDING_FOR_TEXT;

        for (int i = 0; i < boardSize; i++) {
            boxCharBuffer.printAt(gridPaddingLeft + i * (gridSize - 1) + gridSize / 2,gridPaddingTop - 2, String.valueOf(i));
            boxCharBuffer.printAt(gridPaddingLeft - 2,gridPaddingTop + i * (gridSize - 1) + gridSize / 2, String.valueOf(i));

            boxCharBuffer.printAt(gridPaddingLeft + i * (gridSize - 1) + gridSize / 2,gridPaddingTop + boardSize * (gridSize - 1) + 2, String.valueOf(i));
            boxCharBuffer.printAt(gridPaddingLeft + boardSize * (gridSize - 1) + 2,gridPaddingTop + i * (gridSize - 1) + gridSize / 2, String.valueOf(i));

        }
    }

    public static void main(String[] args) {
        FancyOneBoardPrinter fancyBoardPrinter = new FancyOneBoardPrinter(10, 5);

        fancyBoardPrinter.addPlane(Plane.createPlane(new Coordinate(4, 1), Ship.Orientation.S));
        Plane plane = Plane.createPlane(new Coordinate(6, 8), Ship.Orientation.N);
        plane.takeHit(5, 7);
        fancyBoardPrinter.addPlane(plane);
        fancyBoardPrinter.addPlane(Plane.createPlane(new Coordinate(0, 6), Ship.Orientation.E));
        List<HitInfo> hitInfos = new ArrayList<>();
        hitInfos.add(new HitInfo(new Coordinate(5, 5), HitInfo.Type.AIR));
        hitInfos.add(new HitInfo(new Coordinate(4, 1), HitInfo.Type.HEAD));
        hitInfos.add(new HitInfo(new Coordinate(2, 2), HitInfo.Type.BODY));
        fancyBoardPrinter.addOpponentHits(hitInfos);
        fancyBoardPrinter.displayBuffer();
    }

    public void addOpponentHits(List<HitInfo> hitInfos) {
        for (HitInfo hitInfo : hitInfos) {
            markHitInfo(hitInfo);
        }
    }

    private void markHitInfo(HitInfo hitInfo) {
        boxCharBuffer.printAt(PADDING_FOR_TEXT+ hitInfo.getCoordinate().getX() * (gridSize - 1) + gridSize / 2, PADDING_FOR_TEXT+ hitInfo.getCoordinate().getY() * (gridSize - 1) + gridSize / 2, hitInfo.getType().mark);
    }

    public void addPlane(Plane plane) {
        fancyPlanePrinter.drawPlaneOnBuffer(plane, PADDING_FOR_TEXT, PADDING_FOR_TEXT);
    }

    private void addGrid() {
        int gridPaddingLeft = PADDING_FOR_TEXT;
        int gridPaddingTop = PADDING_FOR_TEXT;

        for (int i = 0; i <= boardSize; i++) {
            boxCharBuffer.lineVertical(gridPaddingLeft + i * (gridSize - 1), gridPaddingTop, gridPaddingTop + boardSize * (gridSize - 1), false);
            boxCharBuffer.lineHorizontal(gridPaddingTop + i * (gridSize - 1), gridPaddingLeft, gridPaddingLeft + boardSize * (gridSize - 1), false);
        }
    }

    public String[][] getPrintBuffer() {
        return boxCharBuffer.getPrintBuffer();
    }

    public int getWidth() {
        return boardBufferSizeWidth;
    }

    public int getHeight() {
        return boardBufferSizeHeight;
    }

    public void displayBuffer() {
        boxCharBuffer.outputBuffer();
    }

    public void addMisses(List<Coordinate> misses) {
        for (Coordinate miss : misses) {
            markHitInfo(new HitInfo(miss, HitInfo.Type.AIR));
        }
    }
}
