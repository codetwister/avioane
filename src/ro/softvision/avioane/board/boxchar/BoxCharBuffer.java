package ro.softvision.avioane.board.boxchar;

/**
 * Created by codetwister on 4/25/17.
 */
public class BoxCharBuffer {

    private int width;
    private int height;

    private int[][] boxCharPixels; // flags of box characters
    private String[][] printBuffer;

    public BoxCharBuffer(int width, int height) {
        this.width = width;
        this.height = height;
        boxCharPixels = new int[height][width];
        printBuffer = new String[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                printBuffer[i][j] = " ";
            }
        }
    }

    public void createBox(int x1, int y1, int x2, int y2, boolean d) {
        lineHorizontal(y1, x1, x2, d);
        lineHorizontal(y2, x1, x2, d);
        lineVertical(x1, y1, y2, d);
        lineVertical(x2, y1, y2, d);
    }

    public void lineHorizontal(int y, int x1, int x2, boolean d) {
        if (x2 - x1 > 1) {
            boxCharPixels[y][x1] |= d ? BoxCharPrinter.DRIGHT : BoxCharPrinter.RIGHT;
            for (int i = x1+1; i < x2; i++) {
                boxCharPixels[y][i] |= d ? (BoxCharPrinter.DLEFT | BoxCharPrinter.DRIGHT) : (BoxCharPrinter.LEFT | BoxCharPrinter.RIGHT);
            }
            boxCharPixels[y][x2] |= d ? BoxCharPrinter.DLEFT : BoxCharPrinter.LEFT;
        }
    }

    public void printAt(int x, int y, String what) {
        for (int i = 0; i < what.length(); i++) {
            printBuffer[y][x+i] = what.substring(i, i+1);
        }
    }

    public void lineVertical(int x, int y1, int y2, boolean d) {
        if (y2 - y1 > 1) {
            boxCharPixels[y1][x] |= d ? BoxCharPrinter.DBOTTOM : BoxCharPrinter.BOTTOM;
            for (int i = y1+1; i < y2; i++) {
                boxCharPixels[i][x] |= d ? (BoxCharPrinter.DTOP | BoxCharPrinter.DBOTTOM) : (BoxCharPrinter.TOP | BoxCharPrinter.BOTTOM);
            }
            boxCharPixels[y2][x] |= d ? BoxCharPrinter.DTOP : BoxCharPrinter.TOP;
        }
    }

    public void updateBuffer() {
        for (int i = 0; i < height; i++) {
            String s = "";
            for (int j = 0; j < width; j++) {
                String boxChar = BoxCharPrinter.getBoxChar(boxCharPixels[i][j]);
                printBuffer[i][j] = boxChar.equalsIgnoreCase(" ") ? printBuffer[i][j] : boxChar;
            }
        }
    }

    public String[][] getPrintBuffer() {
        updateBuffer();
        return printBuffer;
    }

    public void outputBuffer() {
        for (int i = 0; i < height; i++) {
            String s = "";
            for (int j = 0; j < width; j++) {
                String boxChar = BoxCharPrinter.getBoxChar(boxCharPixels[i][j]);
                s += boxChar.equalsIgnoreCase(" ") ? printBuffer[i][j] : boxChar;
            }
            System.out.println(s);
        }
    }

    public static void main(String[] args) {
        BoxCharBuffer bcb = new BoxCharBuffer(20, 20);
        bcb.createBox(0, 0, 3, 3, false);
        bcb.createBox(3, 0, 6, 3, false);

        bcb.createBox(0, 3, 3, 6, false);
        bcb.createBox(3, 3, 6, 6, false);

        bcb.createBox(1, 1, 5, 5, true);
        bcb.outputBuffer();
    }
}
