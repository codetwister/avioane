package ro.softvision.avioane.board.boxchar;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by codetwister on 4/13/17.
 */
public class BoxCharPrinter {

    private static Map<Integer, String> charsMap = new HashMap<>();

    static {
        charsMap.put(0b000000, " ");
        charsMap.put(0b00000011, "─"); // DEF 0000 0011 ─ left right
        charsMap.put(0b00000101, "┌"); // DEF 0000 0101 ┌ bottom right
        charsMap.put(0b00000110, "┐"); // DEF 0000 0110 ┐ bottom left
        charsMap.put(0b00000111, "┬"); // DEF 0000 0111 ┬ bottom left right
        charsMap.put(0b00001001, "└"); // DEF 0000 1001 └ top right
        charsMap.put(0b00001010, "┘"); // DEF 0000 1010 ┘ top left
        charsMap.put(0b00001011, "┴"); // DEF 0000 1011 ┴ top lef right
        charsMap.put(0b00001100, "│"); // DEF 0000 1100 │ top bottom
        charsMap.put(0b00001101, "├"); // DEF 0000 1101 ├ top bottom right
        charsMap.put(0b00001110, "┤"); // DEF 0000 1110 ┤ top bottom left
        charsMap.put(0b00001111, "┼"); // DEF 0000 1111 ┼ cross
        charsMap.put(0b00110000, "═"); // DEF 0011 0000 ═ dleft dright
        charsMap.put(0b01010000, "╔"); // DEF 0101 0000 ╔ dbottom dright
        charsMap.put(0b01100000, "╗"); // DEF 0110 0000 ╗ dbottom dleft
        charsMap.put(0b01110000, "╦"); // DEF 0111 0000 ╦ dbottom dleft dright
        charsMap.put(0b10010000, "╚"); // DEF 1001 0000 ╚ dtop dright
        charsMap.put(0b10100000, "╝"); // DEF 1010 0000 ╝ dtop dleft
        charsMap.put(0b10110000, "╩"); // DEF 1011 0000 ╩ dtop dleft dright
        charsMap.put(0b11000000, "║"); // DEF 1100 0000 ║ dtop dbottom
        charsMap.put(0b11010000, "╠"); // DEF 1101 0000 ╠ dtop dbottom dright
        charsMap.put(0b11100000, "╣"); // DEF 1110 0000 ╣ dtop dbottom dleft
        charsMap.put(0b11110000, "╬"); // DEF 1111 0000 ╬ dcross
        charsMap.put(0b01000001, "╓"); // DEF 0100 0001 ╓ dbottom right
        charsMap.put(0b00010100, "╒"); // DEF 0001 0100 ╒ bottom dright
        charsMap.put(0b01000010, "╖"); // DEF 0100 0010 ╖ dbottom left
        charsMap.put(0b00100100, "╕"); // DEF 0010 0100 ╕ bottom dleft
        charsMap.put(0b10000001, "╙"); // DEF 1000 0001 ╙ dtop right
        charsMap.put(0b00011000, "╘"); // DEF 0001 1000 ╘ top dright
        charsMap.put(0b10000010, "╜"); // DEF 1000 0010 ╜ dtop left
        charsMap.put(0b00101000, "╛"); // DEF 0010 1000 ╛ top dleft
        charsMap.put(0b11000011, "╫"); // DEF 1100 0011 ╫ dtop dbottom left right
        charsMap.put(0b00111100, "╪"); // DEF 0011 1100 ╪ top bottom dleft dright
        charsMap.put(0b11000001, "╟"); // DEF 1100 0001 ╟ dtop dbottom right
        charsMap.put(0b00011100, "╞"); // DEF 0001 1100 ╞ top bottom dright
        charsMap.put(0b11000010, "╢"); // DEF 1100 0010 ╢ dtop dbottom left
        charsMap.put(0b00101100, "╡"); // DEF 0010 1100 ╡ top bottom dleft
        charsMap.put(0b01000011, "╥"); // DEF 0100 0011 ╥ dbottom left right
        charsMap.put(0b00110100, "╤"); // DEF 0011 0100 ╤ bottom dleft dright
        charsMap.put(0b10000011, "╨"); // DEF 1000 0011 ╨ dtop left right
        charsMap.put(0b00111000, "╧"); // DEF 0011 1000 ╧ top dleft dright
    }
    // encoding using bitwise operators: top | bottom | left | right

    //    private static final String boxChars =  // 0123 5678 0123 5678
//                                              "┌╔╓╒ ┐╗╖╕ └╚╙╘ ┘╝╜╛ " +
//                                              "│║║│ ─═─═ ┼╬╫╪ ├╠╟╞ " +
//                                              "┤╣╢╡ ┬╦╥╤ ┴╩╨╧ ";

    public static final int TOP = 0b00001000;
    public static final int BOTTOM = 0b00000100;
    public static final int LEFT = 0b00000010;
    public static final int RIGHT = 0b00000001;
    public static final int DTOP = 0b10000000;
    public static final int DBOTTOM = 0b01000000;
    public static final int DLEFT = 0b00100000;
    public static final int DRIGHT = 0b00010000;

    public static String getBoxChar(int flags) {
        return charsMap.containsKey(flags) ? charsMap.get(flags) : " ";
    }


}
