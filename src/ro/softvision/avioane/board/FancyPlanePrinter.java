package ro.softvision.avioane.board;

import ro.softvision.avioane.board.boxchar.BoxCharBuffer;
import ro.softvision.avioane.item.BodyPart;
import ro.softvision.avioane.item.Plane;

/**
 * Created by codetwister on 4/12/17.
 */
public class FancyPlanePrinter {

    private int boardSize;
    private int gridSize;
    private BoxCharBuffer boxCharBuffer;

    public FancyPlanePrinter(int boardSize, int gridSize, BoxCharBuffer boxCharBuffer) {
        this.boardSize = boardSize;
        this.gridSize = gridSize;
        this.boxCharBuffer = boxCharBuffer;
    }

    public void drawPlaneOnBuffer(Plane plane, int gridPaddingTop, int gridPaddingLeft) {
        boolean[][] planeCoordinateTable = new boolean[boardSize][boardSize];

        for (BodyPart bodyPart : plane.getAllParts()) {
            planeCoordinateTable[bodyPart.getCoordinate().getY()][bodyPart.getCoordinate().getX()] = true;
        }

        for (BodyPart bodyPart : plane.getAllParts()) {
            if (bodyPart.isHit()) {
                markCoordinate(bodyPart.getCoordinate(), gridSize, gridPaddingLeft, gridPaddingTop);
            }

            Coordinate coordinate = bodyPart.getCoordinate();
            boolean extendUp = coordinate.getY() > 0 && planeCoordinateTable[coordinate.getY() - 1][coordinate.getX()];
            boolean extendDown = coordinate.getY() < boardSize - 1 && planeCoordinateTable[coordinate.getY() + 1][coordinate.getX()];
            boolean extendLeft = coordinate.getX() > 0 && planeCoordinateTable[coordinate.getY()][coordinate.getX() - 1];
            boolean extendRight = coordinate.getX() < boardSize - 1 && planeCoordinateTable[coordinate.getY()][coordinate.getX() + 1];

            int y1 = gridPaddingTop + coordinate.getY() * (gridSize - 1) + 1;
            int x1 = gridPaddingLeft + coordinate.getX() * (gridSize - 1) + 1;
            int x2 = gridPaddingLeft + coordinate.getX() * (gridSize - 1) + gridSize - 2;
            int y2 = gridPaddingTop + coordinate.getY() * (gridSize - 1) + gridSize - 2;

            if (!extendUp) {
                boxCharBuffer.lineHorizontal(y1, x1, x2, true);
            } else {
                boxCharBuffer.lineVertical(x1, y1 - 2, y1, true);
                boxCharBuffer.lineVertical(x2, y1 - 2, y1, true);
            }
            if (!extendDown) {
                boxCharBuffer.lineHorizontal(y2, x1, x2, true);
            }
            if (!extendLeft) {
                boxCharBuffer.lineVertical(x1, y1, y2, true);
            } else {
                boxCharBuffer.lineHorizontal(y1, x1 - 2, x1, true);
                boxCharBuffer.lineHorizontal(y2, x1 - 2, x1, true);
            }
            if (!extendRight) {
                boxCharBuffer.lineVertical(x2, y1, y2, true);
            }
        }
    }

    private void markCoordinate(Coordinate coordinate, int gridSize, int gridPaddingLeft, int gridPaddingTop) {
        boxCharBuffer.printAt(gridPaddingLeft+ coordinate.getX() * (gridSize - 1) + gridSize / 2, gridPaddingTop + coordinate.getY() * (gridSize - 1) + gridSize / 2, "X");
    }

}
