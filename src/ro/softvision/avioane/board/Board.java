package ro.softvision.avioane.board;

import ro.softvision.avioane.item.BodyPart;
import ro.softvision.avioane.item.Plane;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mihaly.nagy on 10/03/17.
 */
public class Board {

    private List<Plane> planes;
    private List<Plane> remotePlanes;
    private List<HitInfo> remoteHitInfos;
    private List<Coordinate> misses;
    private int boardSize;

    public Board(int boardSize) {
        this.boardSize = boardSize;
        planes = new ArrayList<>();
        remoteHitInfos = new ArrayList<>();
        misses = new ArrayList<>();
        remotePlanes = new ArrayList<>();
    }

    public int getBoardSize() {
        return boardSize;
    }

    public void addPlane(Plane plane) {
        if (planes == null) {
            planes = new ArrayList<>();
        }
        planes.add(plane);
    }

    public boolean hasAnyPlaneLeft() {
        for (Plane plane : planes) {
            if (!plane.isDead()) {
                return true;
            }
        }
        return false;
    }

    public boolean isPlaneOnBoard(Plane plane) {
        for (BodyPart bodyPart : plane.getAllParts()) {
            if (!isCoordinateOnBoard(bodyPart.getCoordinate())) {
                return false;
            }
        }
        return true;
    }

    private boolean isCoordinateOnBoard(Coordinate coordinate) {
        return coordinate.getX() >= 0 && coordinate.getX() < boardSize &&
                coordinate.getY() >= 0 && coordinate.getY() < boardSize;
    }

    public boolean isOverlappingOtherPlane(Plane plane) {
        for (BodyPart bodyPart : plane.getAllParts()) {
            if (getPlaneAt(bodyPart.getCoordinate()) != null) {
                return true;
            }
        }
        return false;
    }

    public void printSelf() {
        Fancy2BoardPrinter fancyBoardPrinter = new Fancy2BoardPrinter(boardSize, 5);

        for (Plane plane : planes) {
            fancyBoardPrinter.addPlane(plane);
        }
        if (remotePlanes != null) {
            fancyBoardPrinter.addRemotePlanes(remotePlanes);
        }
        fancyBoardPrinter.addOpponentHits(remoteHitInfos);
        fancyBoardPrinter.addMisses(misses);
        fancyBoardPrinter.displayBuffer();
    }

    public HitInfo takeHit(Coordinate attackingCoords) {
        HitInfo.Type type;
        for (Plane plane : planes) {
            type = plane.takeHit(attackingCoords.getX(), attackingCoords.getY());
            if (type != HitInfo.Type.AIR) {
                return new HitInfo(attackingCoords, type);
            }
        }
        misses.add(attackingCoords);
        return new HitInfo(attackingCoords, HitInfo.Type.AIR);
    }

    public int getPlaneCount() {
        return planes.size();
    }

    public void addHitInfo(HitInfo hitInfo) {
        remoteHitInfos.add(hitInfo);
    }

    public Plane getPlaneAt(Coordinate coordinate) {
        for (Plane plane : planes) {
            for (BodyPart bodyPart : plane.getAllParts()) {
                if (bodyPart.getCoordinate().equals(coordinate)) {
                    return plane;
                }
            }
        }
        return null;
    }

    public void addRemotePlane(Plane plane) {
        remotePlanes.add(plane);
    }
}
