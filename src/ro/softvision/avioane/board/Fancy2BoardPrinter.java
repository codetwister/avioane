package ro.softvision.avioane.board;

import ro.softvision.avioane.item.Plane;
import ro.softvision.avioane.item.Ship;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by codetwister on 5/5/17.
 */
public class Fancy2BoardPrinter {

    private final FancyOneBoardPrinter playerBoardPrinter;
    private final FancyOneBoardPrinter opponentBoardPrinter;
    private final int height;
    private final int width;

    private String[][] printBuffer;

    public Fancy2BoardPrinter(int boardSize, int gridSize) {
        playerBoardPrinter = new FancyOneBoardPrinter(boardSize, gridSize);
        opponentBoardPrinter = new FancyOneBoardPrinter(boardSize, gridSize);

        height = playerBoardPrinter.getHeight();
        width = playerBoardPrinter.getWidth() * 2;
        printBuffer = new String[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                printBuffer[i][j] = " ";
            }
        }
    }

    public static void main(String[] args) {
        Fancy2BoardPrinter printer = new Fancy2BoardPrinter(10, 5);
        printer.addPlane(Plane.createPlane(new Coordinate(4, 1), Ship.Orientation.S));
        Plane plane = Plane.createPlane(new Coordinate(6, 8), Ship.Orientation.N);
        plane.takeHit(5, 7);
        printer.addPlane(plane);
        printer.addPlane(Plane.createPlane(new Coordinate(0, 6), Ship.Orientation.E));
        List<HitInfo> hitInfos = new ArrayList<>();
        hitInfos.add(new HitInfo(new Coordinate(5, 5), HitInfo.Type.AIR));
        hitInfos.add(new HitInfo(new Coordinate(4, 1), HitInfo.Type.HEAD));
        hitInfos.add(new HitInfo(new Coordinate(2, 2), HitInfo.Type.BODY));
        printer.addOpponentHits(hitInfos);
        printer.displayBuffer();
    }

    private void updateBuffer() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                printBuffer[i][j] = j >= playerBoardPrinter.getWidth() ?
                        opponentBoardPrinter.getPrintBuffer()[i][j - playerBoardPrinter.getWidth()] :
                        playerBoardPrinter.getPrintBuffer()[i][j];
            }
        }
    }

    public void displayBuffer() {
        updateBuffer();
        for (int i = 0; i < height; i++) {
            String s = "";
            for (int j = 0; j < width; j++) {
                s += printBuffer[i][j];
            }
            System.out.println(s);
        }
    }

    public void addOpponentHits(List<HitInfo> hitInfos) {
        opponentBoardPrinter.addOpponentHits(hitInfos);
    }

    public void addPlane(Plane plane) {
        playerBoardPrinter.addPlane(plane);
    }


    public void addRemotePlanes(List<Plane> remotePlanes) {
        for (Plane remotePlane : remotePlanes) {
            opponentBoardPrinter.addPlane(remotePlane);
        }
    }

    public void addMisses(List<Coordinate> misses) {
        playerBoardPrinter.addMisses(misses);
    }
}
