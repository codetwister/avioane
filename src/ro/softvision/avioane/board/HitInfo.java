package ro.softvision.avioane.board;

/**
 * Created by codetwister on 3/30/17.
 */
public class HitInfo {
    Coordinate coordinate;
    Type type;

    public HitInfo(Coordinate coordinate, Type type) {
        this.coordinate = coordinate;
        this.type = type;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Type getType() {
        return type;
    }

    /**
     * Created by codetwister on 3/24/17.
     */
    public enum Type {
        AIR("o"),
        HEAD("X"),
        BODY("+");

        Type(String mark) {
            this.mark = mark;
        }

        public String mark;
    }
}
