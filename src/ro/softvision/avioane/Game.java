package ro.softvision.avioane;

import ro.softvision.avioane.board.Coordinate;
import ro.softvision.avioane.board.HitInfo;
import ro.softvision.avioane.player.Player;

import java.io.IOException;

/**
 * Created by mihaly.nagy on 10/03/17.
 */
public class Game {

    public static final int NUM_OF_OBJECTS = 3;
    private Player player1;
    private Player player2;

    public Game(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public void start() throws IOException {
        System.out.println("Game starting with " + player1.getName() + " and " + player2.getName());
        player1.configureBoard(NUM_OF_OBJECTS);
        player2.configureBoard(NUM_OF_OBJECTS);

        System.out.println("Start!");

        Player winner = null;
        Player currentPlayer = player1;
        do {
            winner = playRound(currentPlayer, currentPlayer == player1 ? player2 : player1);
            currentPlayer = currentPlayer == player1 ? player2 : player1;
        } while (winner == null);
        winner.getPlayerBoard().printSelf();
        System.out.println("The winner is: " + winner.getName());
    }

    private Player playRound(Player attackingPlayer, Player playerUnderAttack) throws IOException {
        Coordinate attackingCoords = attackingPlayer.inputCoordinates();

        Player winner = null;

        HitInfo hitInfo = playerUnderAttack.getPlayerBoard().takeHit(attackingCoords);
        attackingPlayer.getPlayerBoard().addHitInfo(hitInfo);
        if (hitInfo.getType() == HitInfo.Type.HEAD) {
            attackingPlayer.getPlayerBoard().addRemotePlane(playerUnderAttack.getPlayerBoard().getPlaneAt(hitInfo.getCoordinate()));
        }

        if (!playerUnderAttack.getPlayerBoard().hasAnyPlaneLeft()) {
            winner = attackingPlayer;
        }

        playerUnderAttack.displayBoard();

        return winner;
    }
}
