package ro.softvision.avioane;

import ro.softvision.avioane.player.AiPlayer;
import ro.softvision.avioane.player.HumanPlayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by mihaly.nagy on 10/03/17.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Enter your name: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String playerName = br.readLine();
        Game game = new Game(new HumanPlayer(playerName), new AiPlayer("AiPlayer"));
        game.start();
    }
}
