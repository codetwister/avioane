package ro.softvision.avioane.item;

import ro.softvision.avioane.board.Coordinate;

/**
 * Created by codetwister on 3/23/17.
 */
public class BodyPart {
    private Coordinate coordinate;
    private boolean isHit;

    public BodyPart(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public void translate(int x, int y) {
        coordinate.translate(x, y);
    }

    public void rotate(double radians) {
        int newX = (int) Math.round(coordinate.getX() * Math.cos(radians) - coordinate.getY() * Math.sin(radians));
        int newY = (int) Math.round(coordinate.getX() * Math.sin(radians) + coordinate.getY() * Math.cos(radians));
        coordinate = new Coordinate(newX, newY);
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public boolean isHit() {
        return isHit;
    }

    public boolean takeHit(int x, int y) {
        isHit = coordinate.getX() == x && coordinate.getY() == y;
        return isHit;
    }

}
