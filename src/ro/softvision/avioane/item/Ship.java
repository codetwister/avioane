package ro.softvision.avioane.item;

import ro.softvision.avioane.board.Coordinate;
import ro.softvision.avioane.board.HitInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by codetwister on 3/31/17.
 */
public abstract class Ship {

    protected BodyPart head;
    protected List<BodyPart> body;

    protected abstract void createShip();

    public Ship(Orientation orientation, Coordinate coordinate) {
        createShip();
        switch (orientation) {
            case S:
                rotate(Math.PI);
                break;
            case W:
                rotate(-Math.PI / 2);
                break;
            case E:
                rotate(Math.PI / 2);
                break;
        }
        translate(coordinate.getX(), coordinate.getY());
    }

    public List<BodyPart> getAllParts() {
        List<BodyPart> allParts = new ArrayList<>();
        allParts.add(head);
        allParts.addAll(body);
        return allParts;
    }

    public HitInfo.Type takeHit(int x, int y) {
        if (!head.isHit() && head.takeHit(x, y)) {
            return HitInfo.Type.HEAD;
        }
        for (BodyPart bodyPart : body) {
            if (!bodyPart.isHit() && bodyPart.takeHit(x, y)) {
                return HitInfo.Type.BODY;
            }
        }
        return HitInfo.Type.AIR;
    }

    public boolean isDead() {
        return head.isHit();
    }

    protected void translate(int x, int y) {
        head.translate(x, y);
        for (BodyPart bodyPart : body) {
            bodyPart.translate(x, y);
        }
    }

    protected void rotate(double radians) {
        head.rotate(radians);
        for (BodyPart bodyPart : body) {
            bodyPart.rotate(radians);
        }
    }

    public enum Orientation {
        N,
        S,
        W,
        E
    }
}
