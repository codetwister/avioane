package ro.softvision.avioane.item;

import ro.softvision.avioane.board.Coordinate;

import java.util.ArrayList;

/**
 * Created by mihaly.nagy on 10/03/17.
 */
public class Plane extends Ship {

    private Plane(Orientation orientation, Coordinate coordinate) {
        super(orientation, coordinate);
    }

    public static Plane createPlane(Coordinate coordinate, Orientation orientation) {
        return new Plane(orientation, coordinate);
    }

    @Override
    protected void createShip() {
        head = new BodyPart(new Coordinate(0, 0));
        body = new ArrayList<>();
        body.add(new BodyPart(new Coordinate(0, -1)));
        body.add(new BodyPart(new Coordinate(0, -2)));
        body.add(new BodyPart(new Coordinate(0, -3)));

        body.add(new BodyPart(new Coordinate(-2, -1)));
        body.add(new BodyPart(new Coordinate(-1, -1)));
        body.add(new BodyPart(new Coordinate(1, -1)));
        body.add(new BodyPart(new Coordinate(2, -1)));

        body.add(new BodyPart(new Coordinate(-1, -3)));
        body.add(new BodyPart(new Coordinate(1, -3)));
    }
}